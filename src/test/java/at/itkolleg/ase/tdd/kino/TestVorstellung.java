package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.rmi.UnexpectedException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class TestVorstellung {

    private Vorstellung vorstellung;
    @BeforeEach
    void setup(){
        Map<Character, Integer> austroBelg = new HashMap<>();
        austroBelg.put('A', 20);
        austroBelg.put('B', 20);
        austroBelg.put('C', 25);
        austroBelg.put('D', 30);
        KinoSaal kinoSaal = new KinoSaal("Austro", austroBelg );
        Zeitfenster zeitfensterAbend = Zeitfenster.ABEND;
        LocalDate datum = LocalDate.of(2021, 9, 21);
        float preis = 4;
        String film = "Ein echter Wiener geht ned unter";
       vorstellung = new Vorstellung(kinoSaal,zeitfensterAbend, datum, film, preis );
    }

    @Test
    void testName(){
        Assertions.assertEquals(vorstellung.getFilm(), "Ein echter Wiener geht ned unter");
        Assertions.assertNotEquals(vorstellung.getFilm(), "Mundl geht ned unter");
    }

    @Test
    void testSaal(){

        Map<Character, Integer> austroBelg = new HashMap<>();
        austroBelg.put('A', 20);
        austroBelg.put('B', 20);
        austroBelg.put('C', 25);
        austroBelg.put('G', 30);
        KinoSaal testSaalWrongReihenName = new KinoSaal("Austro", austroBelg );

        Map<Character, Integer> guetto = new HashMap<>();
        guetto.put('A', 20);
        guetto.put('B', 20);
        guetto.put('C', 25);
        guetto.put('D', 30);
        KinoSaal testSaalRichtig = new KinoSaal("Austro", guetto );

        Map<Character, Integer> belegWrongNr = new HashMap<>();
        belegWrongNr.put('A', 20);
        belegWrongNr.put('B', 10);
        belegWrongNr.put('C', 20);
        belegWrongNr.put('D', 40);
        KinoSaal testSaalWrongSeatNr = new KinoSaal("Austro", belegWrongNr );

        Map<Character, Integer> belegWrongName = new HashMap<>();
        belegWrongName.put('A', 20);
        belegWrongName.put('B', 20);
        belegWrongName.put('C', 25);
        belegWrongName.put('D', 30);
        KinoSaal testSaalWrongName = new KinoSaal("Esterreich", belegWrongName );

        //Assertions.assertNotEquals(vorstellung.getSaal(), testSaalWrongReihenName);
        Assertions.assertFalse(vorstellung.equals(testSaalWrongSeatNr));
        Assertions.assertFalse(vorstellung.equals(testSaalWrongReihenName));
        Assertions.assertNotEquals(vorstellung.getSaal(), testSaalWrongName);
        Assertions.assertEquals(vorstellung.getSaal(), testSaalRichtig);
    }

}
