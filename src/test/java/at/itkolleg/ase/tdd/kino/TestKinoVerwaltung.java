package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

public class TestKinoVerwaltung {

    private Vorstellung vorstellung;
    private KinoVerwaltung kinoVerwaltung;

    @BeforeEach
    void setup(){
        Map<Character, Integer> austroBelg = new HashMap<>();
        austroBelg.put('A', 20);
        austroBelg.put('B', 20);
        austroBelg.put('C', 25);
        austroBelg.put('D', 30);
        KinoSaal kinoSaal = new KinoSaal("Austro", austroBelg );
        Zeitfenster zeitfensterAbend = Zeitfenster.ABEND;
        LocalDate datum = LocalDate.of(2021, 9, 21);
        float preis = 4;
        String film = "Ein echter Wiener geht ned unter";
        vorstellung = new Vorstellung(kinoSaal,zeitfensterAbend, datum, film, preis );
        kinoVerwaltung = new KinoVerwaltung();
        kinoVerwaltung.einplanenVorstellung(vorstellung);
    }

    @Test
    @DisplayName("Karten kaufen Erfolgreich")
    void testKaufeKarte(){
        Ticket ticketRichtig = kinoVerwaltung.kaufeTicket(vorstellung, 'D', 5, 4);
        Assertions.assertInstanceOf(Ticket.class, ticketRichtig);
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> kinoVerwaltung.kaufeTicket(vorstellung, 'Z', 5, 4));

    }

    @Test
    @DisplayName("Anlegen Vorstellung Erfolgreich")
    void testAnlegenVorstellungIstKorrekt(){
        Map<Character, Integer> austroBelg = new HashMap<>();
        austroBelg.put('A', 20);
        austroBelg.put('B', 20);
        austroBelg.put('C', 25);
        austroBelg.put('D', 30);
        KinoSaal kinoSaal = new KinoSaal("Austro", austroBelg );
        Zeitfenster zeitfensterAbend = Zeitfenster.ABEND;
        LocalDate datum = LocalDate.of(2021, 9, 21);
        float preis = 4;
        String film = "Ein echter Wiener geht ned unter";
        Vorstellung vorstellungRichtig = new Vorstellung(kinoSaal,zeitfensterAbend, datum, film, preis);

        Assertions.assertInstanceOf(Vorstellung.class,vorstellungRichtig);
    }

    @Test
    @DisplayName("Mehrere Vorstellungen erfolgreich angelegt")
    void testAnlegenMehrerVorstellungen(){
        Map<Character, Integer> austroBelg = new HashMap<>();
        austroBelg.put('A', 20);
        austroBelg.put('B', 20);
        austroBelg.put('C', 25);
        austroBelg.put('D', 30);
        KinoSaal kinoSaal = new KinoSaal("Austro", austroBelg );
        Zeitfenster zeitfensterAbend = Zeitfenster.ABEND;
        Zeitfenster zeitfensterNacht = Zeitfenster.NACHT;
        Zeitfenster zeitfensterNmittag = Zeitfenster.NACHMITTAG;
        LocalDate datum = LocalDate.of(2021, 9, 21);
        float preis = 4;
        String film = "Ein echter Wiener geht ned unter";
        Vorstellung vorstellungAbend = new Vorstellung(kinoSaal,zeitfensterAbend, datum, film, preis);
        Vorstellung vorstellungNact = new Vorstellung(kinoSaal,zeitfensterNacht, datum, film, preis);
        Vorstellung vorstellungNmittag = new Vorstellung(kinoSaal,zeitfensterNmittag, datum, film, preis);

        Assertions.assertNotSame(vorstellungAbend, vorstellungNmittag);
        Assertions.assertNotSame(vorstellungAbend, vorstellungNact);
        Assertions.assertNotSame(vorstellungNact, vorstellungNmittag);
    }

    @Test
    @DisplayName("Doppelte Veranstaltung geplant")
    void testDoppelteVeranstaltung(){
        Map<Character, Integer> austroBelg = new HashMap<>();
        austroBelg.put('A', 20);
        austroBelg.put('B', 20);
        austroBelg.put('C', 25);
        austroBelg.put('D', 30);
        KinoSaal kinoSaal = new KinoSaal("Austro", austroBelg );
        Zeitfenster zeitfensterAbend = Zeitfenster.ABEND;
        Zeitfenster zeitfensterNacht = Zeitfenster.NACHT;
        Zeitfenster zeitfensterNmittag = Zeitfenster.NACHMITTAG;
        LocalDate datum = LocalDate.of(2021, 9, 21);
        float preis = 4;
        String film = "Ein echter Wiener geht ned unter";
        Vorstellung vorstellungAbend = new Vorstellung(kinoSaal,zeitfensterAbend, datum, film, preis);
        Vorstellung vorstellungAbend02 = new Vorstellung(kinoSaal,zeitfensterAbend, datum, film, preis);

        KinoVerwaltung kinoVerwaltung = new KinoVerwaltung();
        kinoVerwaltung.einplanenVorstellung(vorstellungAbend);
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> kinoVerwaltung.einplanenVorstellung(vorstellungAbend02) );
    }

    @ParameterizedTest(name = "reihe: {1}, platz:{2}, geld{3}")
    @CsvSource({"A, 5, 10", "B, 3, 5", "C, 6, 4"})
    @DisplayName("Kaufen mehrerer Tickets Erfolgreich")
    void testKaufeMehrereTickets(char reihe, int platz, float geld){
        Ticket ticket = vorstellung.kaufeTicket(reihe, platz, geld);

        Assertions.assertInstanceOf(Ticket.class, ticket);
        Assertions.assertEquals(ticket.getReihe(), reihe);
        Assertions.assertEquals(ticket.getPlatz(), platz);
    }


    @TestFactory
    @DisplayName("Testfactory Dynamisch Tickets kaufen")
    Collection<DynamicTest> testKaufeMehrereTicketsDynamisch(){

        List<DynamicTest> testList = new ArrayList<>();
        Map<Character, Integer> austroBelg = new HashMap<>();
        austroBelg.put('A', 20);
        austroBelg.put('B', 20);
        austroBelg.put('C', 25);
        austroBelg.put('D', 30);
        KinoSaal kinoSaal = new KinoSaal("Austro", austroBelg );
        Zeitfenster zeitfensterAbend = Zeitfenster.ABEND;
        LocalDate datum = LocalDate.of(2021, 9, 21);
        float preis = 4;
        String film = "Ein echter Wiener geht ned unter";
        Vorstellung vorstellungAbend = new Vorstellung(kinoSaal,zeitfensterAbend, datum, film, preis);
        KinoVerwaltung geplant = new KinoVerwaltung();
        geplant.einplanenVorstellung(vorstellung);

        for( int i = 0; i<20; i++){
            Random rand = new Random();
            int preise = rand.nextInt(10) +1;
           int platz = i +1;
            System.out.println(preise);
            if(preise >= 4){
                testList.add(
                DynamicTest.dynamicTest("Richtiger Wert Test",
                        () -> Assertions.assertInstanceOf(Ticket.class, vorstellung.kaufeTicket('A', platz, preise))));
            }else{
                testList.add(
                DynamicTest.dynamicTest("Falscher Wert Test",
                        () -> Assertions.assertThrows(IllegalArgumentException.class,
                                () -> kinoVerwaltung.kaufeTicket(vorstellung, 'Z', platz, preise) )));
            }
        }

        return testList;
    }


}
