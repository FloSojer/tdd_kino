package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class TestKinoSaal {

    KinoSaal kinoSaal;
    @BeforeEach
    void setup(){
        Map<Character, Integer> austroBelg = new HashMap<>();
        austroBelg.put('A', 20);
        austroBelg.put('B', 20);
        austroBelg.put('C', 25);
        austroBelg.put('D', 30);
        kinoSaal = new KinoSaal("Austro", austroBelg );
    }
    @Test
    void testName() {
        Assertions.assertEquals(kinoSaal.getName(), "Austro");
    }

    @Test
    void testPlatz(){
        Assertions.assertTrue(kinoSaal.pruefePlatz('A', 18));
    }

    @Test
    void testNotName(){
        Assertions.assertNotEquals(kinoSaal.getName(), "KinoSaal");
    }

    @Test
    void testKeinPlatz(){
        Assertions.assertFalse(kinoSaal.pruefePlatz('B', 22));
    }
}
