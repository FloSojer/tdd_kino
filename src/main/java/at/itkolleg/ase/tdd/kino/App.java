package at.itkolleg.ase.tdd.kino;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Dieses Beispiel stammt aus https://training.cherriz.de/cherriz-training/1.0.0/testen/junit5.html
 */
public class App 
{
    public static void main( String[] args )
    {
        //Saal anlegen
        Map<Character,Integer> map = new HashMap<>();
        map.put('A',10);
        map.put('B',10);
        map.put('C',15);
        KinoSaal ks = new KinoSaal("LadyX",map);

        Map<Character, Integer> austroBelg = new HashMap<>();
        austroBelg.put('A', 20);
        austroBelg.put('B', 20);
        austroBelg.put('C', 25);
        austroBelg.put('D', 30);
        KinoSaal kinoSaal = new KinoSaal("Austro", austroBelg );

        //Platz prüfen
        System.out.println(ks.pruefePlatz('A',11));
        System.out.println(ks.pruefePlatz('A',10));
        System.out.println(ks.pruefePlatz('B',10));
        System.out.println(ks.pruefePlatz('C',14));

        System.out.println(kinoSaal.pruefePlatz('D', 10));
        System.out.println(kinoSaal.pruefePlatz('A', 30));
        System.out.println(kinoSaal.pruefePlatz('Y', 2));
        System.out.println(kinoSaal.pruefePlatz('B', 2));

        //Vorstellung anlegen
        System.out.println("Neue Vorstellung im Austro Saal");
        Zeitfenster zeitfensterAbend = Zeitfenster.ABEND;
        LocalDate datum = LocalDate.of(2021, 9, 21);
        float preis = 4;
        Vorstellung vorstellung = new Vorstellung(kinoSaal,zeitfensterAbend, datum, "Ein echter Wiener geht ned unter", preis );

        //vorstellung einplanen
        KinoVerwaltung kinoVerwaltung = new KinoVerwaltung();
        kinoVerwaltung.einplanenVorstellung(vorstellung);
       Ticket ticket = kinoVerwaltung.kaufeTicket(vorstellung, 'D', 5, 4);
        System.out.println(ticket.getSaal());
        System.out.println(ticket.getZeitfenster());
        System.out.println(ticket.getDatum());
    }
}
